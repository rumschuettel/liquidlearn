﻿namespace LiquidLearn.UnitTests

open NUnit.Framework
open FsUnit

module Utils =
    open LiquidLearn.Utils

    [<Test>]
    let ``array shuffle shuffles``() =
        let someArray = [|1..1000|]
        let shuffled = Array.copy someArray
        // in place
        shuffle shuffled |> should equal shuffled
        // should be different than before
        shuffled |> should not' (equal someArray)
        // sort it, then should be the same again
        shuffled |> Array.sort |> should equal someArray

    [<Test>]
    let ``list normalization and clamping``() =
        let someList = [ for _ in 0..1000 -> rand.NextDouble() ]
        // can normalize list to 1 by maximum function
        someList
            |> normalizeBy List.max
            |> List.max
            |> should equal 1.0
        // clamp list to range working
        someList
            |> clamp 0.3 0.4
            |> fun l -> (List.min l, List.max l)
            |> should equal (0.3, 0.4)


module CSMat =
    open LiquidLearn.Utils
    open LiquidLearn.Interactions
    open Microsoft.Research.Liquid

    let someMatrix size =
        let e = [| for _ in 1..size -> [| for _ in 1..size -> rand.NextDouble() |] |]
        new CSMat(new CMat(e, e))

    [<TestCase(1, 1)>]
    [<TestCase(1, 17)>]
    [<TestCase(8, 4)>]
    let ``direct sum matrix sizes add``(sizeA : int, sizeB : int) =
        (someMatrix sizeA).Plus(someMatrix sizeB).Length
            |> should equal (sizeA + sizeB)

    [<TestCase(1, 1)>]
    [<TestCase(2, 4)>]
    [<TestCase(12, 16)>]
    let ``direct sum matrix entries in right place``(sizeA : int, sizeB : int) =
        let eA = [| for _ in 1..sizeA -> [| for _ in 1..sizeA -> rand.NextDouble() |] |]
        let eB = [| for _ in 1..sizeB -> [| for _ in 1..sizeB -> -rand.NextDouble() |] |]
        let mA = new CSMat(new CMat(eA, eA))
        let mB = new CSMat(new CMat(eB, eB))
        let m = mA.Plus mB

        let cA, cB = Array.concat eA, Array.concat eB

        for i in 0..sizeA+sizeB-1 do
            for j in 0..sizeA+sizeB-1 do
                if i < sizeA then
                    if j < sizeA then
                        cA |> should contain (m.Item(i, j).r)
                    else
                        m.Item(i, j).MCC |> should equal 0.0
                else
                    if j >= sizeA then
                        cB |> should contain (m.Item(i, j).r)
                    else
                        m.Item(i, j).MCC |> should equal 0.0
    
    [<TestCase("1001")>]
    [<TestCase("0011")>]
    [<TestCase("1")>]
    [<TestCase("0")>]
    let ``state definitions are little-endian``(signature : string) =
        let s = (Data.FromString signature)
        CSMat.Sig2State s
            |> should equal (CSMat.Sig2State (Data.FromString ("0" + signature)))
        (CSMat.Sig2State s) * 2
            |> should equal (CSMat.Sig2State (Data.FromString (signature + "0")))
        CSMat.Sig2State s
            |> should be (lessThan (CSMat.Sig2State (Data.FromString ("1" + signature))))

    [<TestCase(10, "1001", true)>]
    [<TestCase(8, "1001", true)>]
    [<TestCase(8, "1110", false)>]
    let ``matrix Hilbert space extension``(size, signature : string, shouldThrow) =
        let state = Data.FromString signature
        // indirectly tests all basis permutation helpers

        let matrix = (someMatrix size)
        if shouldThrow then
            ( fun() -> matrix.SpreadTo state |> ignore ) |> should throw typeof<System.Exception>

        else
            let spread = matrix.SpreadTo state

            let entriesBefore =
                [ for i in 0..matrix.Length-1 -> [ for j in 0..matrix.Length-1 -> matrix.Item(i, j) ]] |> List.concat |> Set.ofList
            let entriesAfter =
                [ for i in 0..spread.Length-1 -> [ for j in 0..spread.Length-1 -> spread.Item(i, j) ]] |> List.concat |> Set.ofList
        
            // no new entries as we're just shuffling them around (modulo duplicates)
            entriesBefore.IsSubsetOf entriesAfter |> should equal true
            // dimension has to fit
            spread.Length |> should equal (pown 2 (signature.Length))

            // all 1s should be same matrix
            let systems = log2 size |> int
            matrix.SpreadTo (Data.FromString (join "" [for _ in 1..systems -> "1"]))
                |> should equal matrix
            
            // 0111 should be a block matrix in the lower right
            let spread = matrix.SpreadTo (Data.FromString ("0" + (join "" [for _ in 1..systems -> "1"])))
            for i in 0..size-1 do
                for j in 0..size-1 do
                    spread.Item(size + i, size + j) |> should equal (matrix.Item(i, j))

            // 1110 should double every entry
            let spread = matrix.SpreadTo (Data.FromString ((join "" [for _ in 1..systems -> "1"]) + "0"))
            for i in 0..size-1 do
                for j in 0..size-1 do
                    spread.Item(2*i, 2*j) |> should equal (spread.Item(2*i+1, 2*j+1))

    
module Train =
    open LiquidLearn
    open LiquidLearn.Graph
    open LiquidLearn.Interactions
    open LiquidLearn.Interactions.Sets
    open LiquidLearn.Utils
    open Microsoft.Research.Liquid

    // mock interface that generates a zero interaction
    type ZeroInteractionMock() =
        inherit IInteractionFactory()

        override this.ShortForm = "zero interactions"
        override this.GateName list = "zero interaction"
        override this.Names n = [join "" [for _ in 1..n -> "0"], n] // return one 0 interaction between all vertices
        override this.Matrix name = fun _ -> new CSMat(new CMat(pown 2 (name.Length), false))


    [<TestFixture>]
    type MultiControlModelFixture() = class

        [<SetUp>]
        member this.SetUp() =
            // liquid wants to write out circuits
            System.IO.Directory.SetCurrentDirectory(System.IO.Path.GetTempPath())

        [<TearDown>]
        member this.TearDown() = 
            ()

        [<Test>]
        member this.``train and test run without data``() =
            let graph = new Hypergraph([ O"1" --- O"2" ])
            let trainer = new Train.MultiControlModel(graph, ZeroInteractionMock())
            let dataset = { Data.Yes = []; Data.No = [] }

            trainer.Train(dataset, fun tag ket ->
                if tag <> "test" then
                    // expect the simulation to use 3 qubits, as we need to control one interaction
                    ket.Qubits.Length |> should equal 3
                    // no bias, as there's no data
                    System.Math.Round(ket.Qubits.Item(0).Prob1, 6) |> should equal 0.5
                    System.Math.Round(ket.Qubits.Item(1).Prob1, 6) |> should equal 0.5
                    System.Math.Round(ket.Qubits.Item(2).Prob1, 6) |> should equal 0.5

                ket
            ).Test(dataset) |> should equal { Train.YesStats = []; Train.NoStats = [] }

        [<TestCase("10", "01")>]
        [<TestCase("110", "100")>]
        [<TestCase("111", "000")>]
        [<TestCase("0101", "0100")>]
        member this.``training non-coupled model``(high : string, low : string) =
            // create graph with one big edge containing high.length output vertices
            let graph = new Hypergraph([[ for v in 1..high.Length -> O(string v) ] |> Set.ofList |> Graph.UE])

            let yes, no = Data.FromString <|| (high, low)
            let trainer = new Train.MultiControlModel(graph, ZeroInteractionMock())
            let dataset = { Data.Yes = [yes]; Data.No = [no] }

            trainer.Train(dataset, fun tag ket ->
                ket.Qubits.Length |> should equal (high.Length + 1)

                // first qubit is control qubit, last few are the ones the projector acts on

                // approximately no control bias, as the interaction is zero
                System.Math.Round(ket.Qubits.Item(0).Prob1, 6) |> should equal 0.5
                dump [ for i in 0..high.Length -> ket.Qubits.Item(i).Prob1 ]             

                // for some reason, !! conflicts with the testing framework
                // so we trust in MeasurementStatistics, which is simple enough
                Train.ControlMeasurement.MeasurementStatistics ket [1..high.Length]
                    |> fun probs ->
                        dump probs
                        if tag = "MCM YES" then
                            probs.[CSMat.Sig2State yes] |> should be (greaterThan 0.995)
                        elif tag = "MCM NO" then
                            probs.[CSMat.Sig2State no] |> should be (greaterThan 0.995)

                dump [ for i in 0..high.Length -> ket.Qubits.Item(i).Prob1 ]             

                ket
            ) |> ignore

        [<TestCase("1", "0", "YES", "1", 1)>]
        [<TestCase("1", "0", "NO", "1", 0)>]
        [<TestCase("11", "01", "YES", "11", 2)>]
        [<TestCase("101", "0+1", "NO", "011", 2)>]
        member this.``learning projections``(yes : string, no : string, tag : string, interaction : string, weight : float) =
            let graph = new Hypergraph([[ for v in 1..yes.Length -> O(string v) ] |> Set.ofList |> Graph.UE])
            let trainer = new Train.MultiControlModel(graph, Sets.Projectors())
            let dataset = {
                Data.Yes = Data.FromString <|| [ yes ]
                Data.No  = Data.FromString <|| [ no ]
            }

            let cEdge = trainer.TrainingGraph.Edges.[0]
            let rnd = fun (x : float) -> System.Math.Round(x, 3)

            trainer.Train(dataset, fun t ket ->
                dump trainer.TrainingGraph
                if t = (sprintf "MCM %s" tag) then
                    let controlQubits = trainer.TrainingGraph.WhichControlQubits cEdge
                    let measurement = Train.ControlMeasurement.MeasurementStatistics ket controlQubits
                    let controls = match cEdge with | CE e -> Train.ControlMeasurement.InterpretControlMeasurement measurement e | _ -> failwith "should not happen"

                    controls |> List.find (fun (e, w) -> e.name = interaction) |> snd |> rnd |> should equal weight

                ket            
            ) |> ignore

            ()

    end


    [<TestFixture>]
    type SingleControlModelFixture() = class

        [<SetUp>]
        member this.SetUp() =
            // liquid wants to write out circuits
            System.IO.Directory.SetCurrentDirectory(System.IO.Path.GetTempPath())

        [<TearDown>]
        member this.TearDown() = 
            ()

        [<Test>]
        member this.``train and test run without data``() =
            let graph = new Hypergraph([ O"1" --- O"2" ])
            let trainer = new Train.SingleControlModel(graph, ZeroInteractionMock())
            let dataset = { Data.Yes = []; Data.No = [] }

            trainer.Train(dataset, epochs = 2, postSelector = fun tag ket ->
                if tag <> "test" then
                    // expect the simulation to use 3 qubits, as we need to control one interaction
                    ket.Qubits.Length |> should equal 3
                    // no bias, as there's no data
                    System.Math.Round(ket.Qubits.Item(0).Prob1, 6) |> should equal 0.5
                    System.Math.Round(ket.Qubits.Item(1).Prob1, 6) |> should equal 0.5
                    System.Math.Round(ket.Qubits.Item(2).Prob1, 6) |> should equal 0.5

                ket
            ).Test(dataset) |> should equal { Train.YesStats = []; Train.NoStats = [] }

        [<TestCase("10", "01")>]
        [<TestCase("110", "100")>]
        [<TestCase("111", "000")>]
        [<TestCase("0101", "0100")>]
        member this.``training non-coupled model``(high : string, low : string) =
            // create graph with one big edge containing high.length output vertices
            let graph = new Hypergraph([[ for v in 1..high.Length -> O(string v) ] |> Set.ofList |> Graph.UE])

            let yes, no = Data.FromString <|| (high, low)
            let trainer = new Train.SingleControlModel(graph, ZeroInteractionMock())
            let dataset = { Data.Yes = [yes]; Data.No = [no] }

            trainer.Train(dataset, epochs = 2, postSelector = fun tag ket ->
                ket.Qubits.Length |> should equal (high.Length + 1)

                // first qubit is control qubit, last few are the ones the projector acts on

                // approximately no control bias, as the interaction is zero
                System.Math.Round(ket.Qubits.Item(0).Prob1, 6) |> should equal 0.5

                // for some reason, !! conflicts with the testing framework
                // so we trust in MeasurementStatistics, which is simple enough
                Train.ControlMeasurement.MeasurementStatistics ket [1..high.Length]
                    |> fun probs ->
                        dump probs
                        if tag = "SCM YES" then
                            probs.[CSMat.Sig2State yes] |> should be (greaterThan 0.995)
                        elif tag = "SCM NO" then
                            probs.[CSMat.Sig2State no] |> should be (greaterThan 0.995)

                dump [ for i in 0..high.Length -> ket.Qubits.Item(i).Prob1 ]             

                ket
            ) |> ignore
        
        [<Test>]
        member this.``suspending and resuming``() =
            let graph = new Hypergraph([ O"1" --- O"2" ])
            let trainer = new Train.SingleControlModel(graph, Sets.Projectors())
            let dataset = { Data.Yes = Data.FromString <|| [ "00" ]; Data.No = Data.FromString <|| [ "10" ] }

            trainer.Train(dataset, epochs = 2) |> ignore

            let ms = Utils.IO.Pickle(trainer.TrainedGraph)
            let graph2 : Option<Hypergraph> = Utils.IO.Unpickle(ms)

            graph2.Value.DisplayForm |> should equal trainer.TrainedGraph.Value.DisplayForm

            let trainer2 = new Train.SingleControlModel(graph2.Value, Sets.Projectors())
            trainer2.Train(dataset, epochs = 2) |> ignore

            trainer2.TrainedGraph.Value.DisplayForm |> should not' (equal graph2.Value.DisplayForm)

    end