﻿// LiquidLearn (c) 2016, 2017 Johannes Bausch
module LiquidLearn.Graph

open LiquidLearn.Utils

// recursive vertex type
[<StructuredFormatDisplay("{DisplayForm}")>]
type VertexT =
    | V of string
    | O of string
    with
    member this.DisplayForm =
        match this with
        | V v -> v
        | O o -> "*" + o


type UncoupledEdgeT = Set<VertexT>
type StaticEdgeT = {
    name : string
    amplitude : float
    vertices : UncoupledEdgeT
}
type ControlledEdgeT = StaticEdgeT list

// generic edge
[<StructuredFormatDisplay("{DisplayForm}")>]
type EdgeT = 
    | UE of UncoupledEdgeT
    | SE of StaticEdgeT
    | CE of ControlledEdgeT
    with
    member this.DisplayForm = 
        match this with
        | UE _ ->
            join "──" (this.NormalOrder ||> sprintf "%A")
        | SE edge ->
            sprintf "[%s, %.2f]: %A" edge.name edge.amplitude (UE edge.vertices)
        | CE edges ->
            sprintf "%s: %s"
                (join "══" (this.NormalOrder ||> sprintf "%A"))
                (join " " (edges ||> SE ||> sprintf "%A"))

    member this.Vertices =
        match this with
        | UE vertices -> vertices
        | SE edge -> edge.vertices
        | CE edges -> Set.unionMany (edges ||> (fun e -> e.vertices))

    // return a sorted vertex list
    member this.NormalOrder =
        this.Vertices |> Set.toList |> List.sort

    member this.HowManyInteractions =
        match this with
        | UE _ -> 0
        | SE _ -> 1
        | CE edges -> edges.Length

    member this.Size =
        match this with
        | UE _ | SE _ -> this.Vertices.Count
        | CE _ ->
            this.Vertices.Count + (nextExponentOf2 (this.HowManyInteractions + 1))

    member this.Subsets = powerset this.NormalOrder |> Seq.groupBy (fun el -> el.Length) |> Seq.toList ||> snd



// F# does not support overloading non-tuple operators
let inline (---) a b = 
    (
        match box a, box b with
        | (:? EdgeT as a), (:? EdgeT as b) -> Set.union a.Vertices b.Vertices
        | (:? VertexT as a), (:? EdgeT as b) -> b.Vertices.Add a
        | (:? EdgeT as a), (:? VertexT as b) -> a.Vertices.Add b
        | (:? VertexT as a), (:? VertexT as b) -> Set.ofList [a; b]
        | _ -> failwith "not a valid hyperedge type"
    )
    ||> unbox<VertexT>
    |> UE

// helper types for graph
exception WouldSplitEdgeException
exception NoOptimalGraphFound

[<StructuredFormatDisplay("Hypergraph {DisplayForm}")>]
type Hypergraph(edges : EdgeT list) = class
    // first find any control vertices that might be necessary
    let controls = edges |> List.choose (function | CE e -> Some e | _ -> None)
    // static interactions that are not controlled; we'll have to add them back
    let statics = edges |> List.choose (function | SE e -> Some (SE e) | _ -> None)

    // extract unique vertices and put into list for unique ordering, outputs and controls
    let vertices = [for e in edges -> e.Vertices] |> Set.unionMany |> Set.toList
    let outputs = vertices |> List.filter (function O _ -> true | _ -> false)


    // check that all output vertices are contained in vertex list
    do
        if (Set.difference (Set.ofList outputs) (Set.ofList vertices)).Count > 0 then failwith "some disconnected output vertices"

    // create a map control edge -> qubit number
    // for a control with 3 and one with 5 interactions, we'd map to [0;1] and [2;3;4]
    let controlLookupTable =
        List.zip controls (
            // qubit sizes
            let controlQubitSizes = [ for c in controls -> nextExponentOf2 ((CE c).HowManyInteractions+1) ]
            // offsets
            let offsets =
                controlQubitSizes
                |> (List.scan (+) 0)
                |> List.rev
                |> List.tail
                |> List.rev
            
            [ for i, o in List.zip controlQubitSizes offsets -> [o..o+i-1] ]
        )
        |> Map.ofList

    // number of control qubits
    let controlQubitCount = 
        if controlLookupTable.Count > 0 then
            controlLookupTable
                |> Map.toList
                    ||> snd
                |> List.concat
                |> List.max
                |> (+) 1
        else
            0

    // create a map vertex -> qubit number, V"1", O"...", V"2" we would have
    // V"1" -> 0, O"..." -> 2 and V"2" -> 3
    let qubitLookupTable =
        List.zip vertices [controlQubitCount..controlQubitCount+vertices.Length-1] |> Map.ofList

    // total number of qubits required to allocate for this graph
    let qubitCount = controlQubitCount + qubitLookupTable.Count

    
    // allows to look up a unique qubit for vertices or an edge
    // the edge qubits are normal-ordered, as edges are sets where a control can have a specific position which we want to be unambiguous
    // the vertex list is unordered, so that the qubits for a; b are 0; 1 but for b; a it's 1; 0
    member this.WhichQubits (vertex : VertexT) = qubitLookupTable.[vertex]
    member this.WhichQubits (vertices : VertexT list) = vertices ||> this.WhichQubits
    member this.WhichQubits (edge : EdgeT) =
        match edge with
        | UE _ | SE _ ->
            edge.NormalOrder |> this.WhichQubits
        | CE e ->
            (this.WhichControlQubits edge) @ (edge.NormalOrder |> this.WhichQubits)
    member this.WhichControlQubits = function | CE e -> controlLookupTable.[e] | _ -> []

    member this.DisplayForm =
        join "" (
            "\n  vertices:\n    " :: (List.zip vertices (vertices ||> this.WhichQubits) ||> sprintf "%A ") @
            "\n  controls:\n" :: (controls ||> fun c -> sprintf "    %A (%A) \n" (CE c) (this.WhichQubits (CE c))) @
            "  static:\n" :: (statics ||> sprintf "    %A \n")
        )

    member this.ShortForm = sprintf "%dV%dE-%x" vertices.Length edges.Length (abs(hash edges) % (16*16*16))

    // vertices and edges
    member this.Edges = edges
    member this.Vertices = vertices
    member this.Outputs = outputs
    member this.Controls = controls
    member this.Size = qubitCount

    // return an optimized graph
    member this.OptimizeControls
        (
            maxInteractions : int,
            maxVertices : int,
            ?avoidSplittingEdges : bool,
            ?deleteDuplicateInteractions : bool
        ) =
        let avoidSplittingEdges = defaultArg avoidSplittingEdges true
        let deleteDuplicateInteractions = defaultArg deleteDuplicateInteractions true
        // this is a variant of integer list partitioning, which is known to be NP-hard
        // we use a poly-time greedy approximation, known to lie within 7/6 of the optimal solution

        // extract a list of all interactions in the graph, delete potential duplicates and shuffle
        let interactions =
            this.Controls
            |> List.concat // now a StaticEdgeT list
            |> fun interactions ->
                if deleteDuplicateInteractions then
                    interactions |> Set.ofList |> Set.toArray
                else
                    interactions |> List.toArray
            |> shuffle
            |> Array.toList

        // count vertices in interaction sequence
        let countVertices interactions =
            interactions
            ||> fun el -> el.vertices
            |> Seq.concat
            |> Set.ofSeq
            |> Set.count

        // gives a score of adding the interaction to an array
        let score (interaction : StaticEdgeT) (list : StaticEdgeT list) =
            let listContainsEdge =
                if avoidSplittingEdges then
                    (list |> List.tryFind (fun l -> l.vertices = interaction.vertices)).IsSome
                else
                    false // ignore this check and bunch edges together at will
            // too many interactions in partition
            if list.Length >= maxInteractions then
                if listContainsEdge then raise WouldSplitEdgeException
                None
            // if the list already has an interaction between the given vertices, we want to add it to this list
            elif listContainsEdge then Some 1000
            // fill up empty arrays first, only if there's no way of adding an interaction to a list without increasing its score
            // this guarantees that a partitioning *will* be found eventually, when we distribute each interaction to its own edge
            elif list.Length = 0 then Some -1
            // score is difference in unique vertex count
            else
                let before = countVertices list
                let after  = countVertices (interaction :: list)
                // too many vertices?
                if after > maxVertices then None
                else Some (2 * (before - after))

        // try distributing the interactions to a partition of size n
        let rec tryPartition = function
            | n when n < 0 || n > interactions.Length -> raise NoOptimalGraphFound
            | n ->
                let partition : StaticEdgeT list [] = [| for i in 1..n -> [] |]

                let rec tryDistribute = function
                    | [] -> true
                    | interaction::tail ->
                        shuffle partition |> ignore
                        // find all scores for adding interaction to each possible urn
                        // we don't care about tail recursion as we will not have a lot of calls
                        try
                            let scores = score interaction <|| partition

                            // can't add to any part? break.
                            if scores |> Array.forall (fun score -> score = None) then false
                            // otherwise, add to the item with highest score
                            else
                                let maxScore =
                                    scores
                                    |> Array.filter (function None -> false | _ -> true)
                                    |> Array.max

                                let maxScoreIndex = scores |> Array.findIndex (fun s -> s = maxScore)
                                partition.[maxScoreIndex] <- interaction :: partition.[maxScoreIndex]
                                tryDistribute tail
                        with
                        | :? WouldSplitEdgeException -> false

                
                match tryDistribute interactions with
                | true ->
                    // return with good partition
                    partition
                | false ->
                    // try with one more edge
                    tryPartition (n+1)
        
        // try building new graph with optimized interactions
        // since this is a stochastic algorithm, we try a few times and take the most optimal result
        try
            let g =
                [ for _ in 1..250 ->
                    new Hypergraph([ for part in tryPartition 0 -> CE part ] @ statics)
                ] |> List.minBy (fun g -> g.Size)
            dumps (sprintf "optimized graph with %d edge%s and %d qubit%s found" g.Edges.Length (pluralS g.Edges.Length) g.Size (pluralS g.Size) )
            g
        with
        | NoOptimalGraphFound ->
            dumps "WARN: no more optimal graph within restricted parameters found."
            this

end

