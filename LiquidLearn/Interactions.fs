﻿// LiquidLearn (c) 2016, 2017 Johannes Bausch
module LiquidLearn.Interactions

open Microsoft.Research
open Liquid.Operations // for >< and !! operator

open LiquidLearn.Utils
open System

module Matrices =
    // some standard matrices
    let PauliX = Liquid.CSMat(2, [0, 1, 1.0, 0.0; 1, 0, 1.0, 0.0])
    let PauliY = Liquid.CSMat(2, [0, 1, 0.0, -1.0; 1, 0, 0.0, 1.0])
    let PauliZ = Liquid.CSMat(2, [0, 0, 1.0, 0.0; 1, 1, -1.0, 0.0])
    let Identity = Liquid.CSMat(2, [0, 0, 1.0, 0.0; 1, 1, 1.0, 0.0])
    let IdentityN n = Liquid.CSMat(n, [ for i in 0..n-1 -> i, i, 1.0, 0.0 ])
        

type Liquid.CSMat with
    // direct sum
    member this.Plus (other : Liquid.CSMat) =
        let totalSize = this.Length + other.Length
        let thisEntries = [
            for i in 0..this.Length-1 do
                for j in 0..this.Length-1 do
                    let entry = this.Item (i, j)
                    yield (i, j, entry.r, entry.i)
        ]
        let otherEntries = [
            for i in 0..other.Length-1 do
                for j in 0..other.Length-1 do
                    let entry = other.Item (i, j)
                    yield (i + this.Length, j + this.Length, entry.r, entry.i)
        ]
        
        Liquid.CSMat(totalSize, List.concat [thisEntries; otherEntries])

    // basis permutation helpers
    // Sig2State (FromString "011") = 3
    static member Sig2State (signature : Data.DataT) = 
        let signature = signature |> List.rev
        [ for i in 0..signature.Length-1 -> if signature.[i] = Data.BitT.One then pown 2 i else 0 ] |> List.sum

    // |001><010|
    static member Rank1Projector dim (sigA, sigB) =
        let stateA, stateB = (sigA, sigB) ||> Liquid.CSMat.Sig2State
        Liquid.CSMat(dim, [(stateA, stateB, 1., 0.)])

    static member RankNProjector dim (sigs : (Data.StateT * Data.StateT) list) =
        sigs
            ||> Liquid.CSMat.Rank1Projector dim
            ||> fun m -> m.Dense()
            |> List.reduce (fun acc m -> acc.Add(m); acc)
            |> Liquid.CSMat

    static member BasisPermutationMatrix permutation systems =
        let basis = [Data.BitT.Zero; Data.BitT.One] |> tuples systems
        let permutedBasis = basis ||> List.permute permutation

        Liquid.CSMat.RankNProjector (pown 2 systems) (List.zip basis permutedBasis)

    // permutation of basis
    member this.PermuteBasis permutation =
        if (log2 this.Length) % 1.0 <> 0.0 then failwith "not a power of 2 matrix size"
        let systems = log2 this.Length |> round |> int
        let permutationMatrix = Liquid.CSMat.BasisPermutationMatrix permutation systems

        permutationMatrix.Adj().Mul(this).Mul(permutationMatrix)

    // take a matrix and act with it on qubits specified by 1s in a list, i.e.
    // M on [0; 1; 1; 0; 0] will be transformed to 1 otimes M otimes 1^3
    member this.SpreadTo (signature : Data.StateT) =
        if (log2 this.Length) % 1.0 <> 0.0 then failwith "not a power of 2 matrix size"
        let systems = log2 this.Length |> round |> int
        let other = signature.Length - systems
        match other with
        | n when n < 0 -> failwith "cannot spread matrix to fewer qubits"
        | 0 -> this
        | n ->
            let bigmatrix = this.Kron (pown 2 (signature.Length - systems))
            let orig = [for i in 1..systems -> Data.BitT.One ] @ [for i in 1..n -> Data.BitT.Zero ]
            bigmatrix.PermuteBasis (getPermutation orig signature)


// Training Gates

// projector gate
let Projector count (states : (float * Data.DataT) list) (theta : float) (qs : Liquid.Qubits) =
    // expand indeterminate states
    let statesExpanded = [
        for w, state in states do
            yield! [
                for s in Data.ExpandIndeterminate state -> (w, s)
            ]
    ]
    // in case two or more identical states are given, we simply pick the strongest one
    let statesExpanded = List.sortBy fst statesExpanded

    // overall matrix size
    let size = pown 2 count
    // create diagonal
    let entries = statesExpanded ||> (fun (strength, state) -> (Liquid.CSMat.Sig2State state, strength)) |> Map.ofList
    let diagonal =
        [
            for i in 0..size-1 ->
                if entries.ContainsKey i then
                    let strength = entries.[i]
                    (i, i, Math.Cos(strength * theta), Math.Sin(strength * theta))
                else
                    (i, i, 1.0, 0.0)
        ]

    // create new liquid gate and run
    (new Liquid.Gate(
        Name = "Projector",
        Draw = (join "" [ for i in 0..qs.Length-1 -> sprintf "\\cwx[#%d]\\go[#%d]\\gate{Proj}" i i ]),
        Help = sprintf "custom projector onto %A" states,
        Mat  = Liquid.CSMat(size, diagonal)
    )).Run qs

// gate from matrix
let MatrixInteraction name (matrix : float -> Liquid.CSMat) theta (qs : Liquid.Qubits) =
    (new Liquid.Gate(
        Name = name,
        Draw = sprintf "\\multigate{#%d}{%s}" (qs.Length-1) name,
        Mat  = matrix theta
    )).Run qs


// like CgateNC, but for a qudit control; we assume all matrices are the same size
let ControlledInteraction name (matrices : (float -> Liquid.CSMat) list) theta (qs : Liquid.Qubits) =
    // size of single matrix block
    let singleMatrixSize = (matrices.[0] 0.0).Length
    // size of block of matrices to control
    let usefulMatrixSize = matrices.Length * singleMatrixSize
    // next power of 2, need at least one identity matrix, hence the +1
    let totalSize = nextPowerOf2 (usefulMatrixSize + singleMatrixSize)

    let identity = Matrices.IdentityN (totalSize - usefulMatrixSize)
    let matrices = (fun matrix -> matrix theta) <|| matrices

    // direct sum all matrices to 1 + m1+m2+...+mn
    let matrixSum = matrices |> List.fold (fun (bigmatrix : Liquid.CSMat) matrix -> bigmatrix.Plus matrix) identity

    (new Liquid.Gate(
        Name = "QuditControl",
        Draw = (
            let numberOfControls = nextExponentOf2 (matrices.Length + 1)
            let numberOfQubits = singleMatrixSize |> log2 |> round |> int
            let controlString = join "" [ for i in 0..numberOfControls-1 -> sprintf "\\qwx[#%d]\\go[#%d]\\control" i i ]
            controlString + (sprintf "\qwx[#%d]\\go[#%d]\\multigate{#%d}{%s}" (numberOfControls+numberOfQubits-1) numberOfControls (numberOfControls+numberOfQubits-1) name )
        ),
        Mat = matrixSum
    )).Run qs

// some interaction sets
module Sets =
    type InteractionT = (float -> Liquid.Qubits -> unit)

    // Interaction base class
    [<AbstractClass>]
    [<StructuredFormatDisplay("{ShortForm}")>]
    type IInteractionFactory() =
        abstract member ShortForm : string
        // give back gate name for a list of interactions
        abstract member GateName : Graph.StaticEdgeT list -> string
        // give a complete list of possible interactions * number of qubits it acts on
        abstract member Names : int -> (string * int) list
        // for any id returned by Names, this function has to return a valid matrix
        // the size of the matrix has to be n qubits, where n is given to Names to yield the id.
        abstract member Matrix : string -> (float -> Liquid.CSMat)

        // for every edge, pass a list of interactions and the vertices within that edge
        // that the interaction is acting on nontrivially
        // the locality given for every name has to equal the size of the matrix returned by (Matrix name) above
        member this.ListPossibleInteractions (edge : Graph.EdgeT) =
            match edge with
            | Graph.UE _ ->
                let whole = edge.NormalOrder
                let subsystems = edge.Subsets
                [ for name, locality in this.Names edge.Size ->
                    [ for vertices in subsystems.[locality] ->
                      {
                        Graph.StaticEdgeT.name = name
                        Graph.StaticEdgeT.vertices = Graph.UncoupledEdgeT vertices
                        Graph.StaticEdgeT.amplitude = 1.0
                    } ]
                ] |> List.concat
            | Graph.SE e -> [e]
            | _ -> failwith "controlled edges given to trainer"

        // simply gives back one matrix interaction for an interaction name
        member this.Interaction (edge : Graph.StaticEdgeT) =
            MatrixInteraction (sprintf "%s on %A" edge.name (Graph.UE edge.vertices)) (this.Matrix edge.name)

        // if we have a controlled edge, gives back a big controlled interaction matrix
        // for all the interactions in the edge.control.interactions list
        // otherwise just returns a list of static interaction matrices, one for each
        // possible interaction on that edge.
        member this.Interaction (edge : Graph.ControlledEdgeT) =
            let whole = (Graph.CE edge).NormalOrder
            let matrices =
                [ for interaction in edge ->
                    // if interaction.vertices = 1; 2 and whole = 0; 1; 2; 3; 4 then signature = 01100
                    let signature =
                        [ for v in whole ->
                            if contains v (Graph.UE interaction.vertices).NormalOrder then
                                Data.BitT.One
                            else
                                Data.BitT.Zero
                        ]
                    let matrix = this.Matrix interaction.name
                    fun theta -> (matrix (theta*(interaction.amplitude))).SpreadTo signature
                ]
            
            ControlledInteraction (this.GateName edge) matrices

        member this.Interaction (edge : Graph.EdgeT) =
            match edge with
            | Graph.SE e -> this.Interaction e
            | Graph.CE e -> this.Interaction e
            | Graph.UE _ -> failwith "uncoupled edge has no interaction"


    // Pauli matrix products
    type Paulis() =
        inherit IInteractionFactory()
        override this.ShortForm = "Paulis"

        override this.GateName list = sprintf "%d Paulis" list.Length

        override this.Names n = [ for name in tuples n ['0'; '1'; '2'; '3'] -> string (new System.String(Seq.toArray name)), n ]
        override this.Matrix name = GeneratedCode.PauliProductMatrices.Get name

    // Projectors
    type Projectors() =
        inherit IInteractionFactory()

        override this.ShortForm = "Projectors"

        override this.GateName list = sprintf "%d Projectors" list.Length

        override this.Names n = [ for name in tuples n ['0'; '1'] -> string (new System.String(Seq.toArray name)), n ]
        override this.Matrix name = GeneratedCode.Rank1ProjectionMatrices.Get name

    // Reduced Projectors
    type ReducedProjectors() =
        inherit IInteractionFactory()

        override this.ShortForm = "RedProjectors"

        override this.GateName list = sprintf "%d RedProjectors" list.Length

        override this.Names n = [("flip", 2); ("00", 2); ("11", 2)]
        override this.Matrix name = GeneratedCode.ReducedProjectionMatrices.Get name

    // Compressed Random Matrices
    // set of 7 random sparse Hermitian matrices
    // These are 2-local 3-qubit interactions or 2-local 2-qubit
    type Random() =
        inherit IInteractionFactory()

        override this.ShortForm = "Random"

        override this.GateName list = sprintf "%d Random" list.Length

        override this.Names n =
            match n with
            | n when n >= 2 -> GeneratedCode.SparseRandomHermitian.List ||> fun name -> name, 2
            | _ -> failwith "History State matrices are 2-local"

        override this.Matrix name = GeneratedCode.SparseRandomHermitian.Get name

    // History State Matrices
    // set of 7 Hadamard-type matrices from History state constructions, all 2-local
    type History() =
        inherit IInteractionFactory()

        override this.ShortForm = "History"

        override this.GateName list = sprintf "%d History" list.Length

        override this.Names n =
            match n with
            | n when n >= 2 -> GeneratedCode.UniversalHistory.List ||> fun name -> name, 2
            | _ -> failwith "History State matrices are 2-local"

        override this.Matrix name = GeneratedCode.UniversalHistory.Get name

    // Heisenberg a XX + b YY + c ZZ, or 3-local variant
    type Heisenberg() =
        inherit IInteractionFactory()

        override this.ShortForm = "Heisenberg"

        override this.GateName list = sprintf "%d Heisenberg" list.Length

        override this.Names n =
            [["1"]; ["2"]; ["3"]]
                ||> tuples n
                |||> join ""
                |> List.concat
                ||> fun a -> (a, n)

        override this.Matrix name = GeneratedCode.PauliProductMatrices.Get name

    // Ising a XX + b Z
    type Ising() =
        inherit IInteractionFactory()

        override this.ShortForm = "Ising"

        override this.GateName list = sprintf "%d Ising" list.Length

        override this.Names n = [("1", 1); ("33", 2)]

        override this.Matrix name = GeneratedCode.PauliProductMatrices.Get name
