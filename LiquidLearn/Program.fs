﻿// LiquidLearn (c) 2016, 2017 Johannes Bausch

namespace Microsoft.Research.Liquid

module LearnApp =
    open LiquidLearn
    open LiquidLearn.Utils

    open Util
    open System
    open System.IO
    open System.Diagnostics

    // splits list in two
    let SplitYesNo (list : 'a list) = [List.take (list.Length/2) list; List.skip (list.Length/2) list]

    // all possible unique data sets invariant under the given symmetries, plus yes/no symmetry
    // for some reason, writing all the pipeline in one massively slows down compilation,
    // to the extend of making it unusable. This sounds like a bug in the F# compiler.
    // Note that this function grows superfactorially, so don't feed it more than 3.
    let AllDataSets n (symmetries : (int list * int list) list) =
        let symmetries = id :: (symmetries ||> fun (a, b) -> getPermutation a b)

        let allPossible =
            ["0"; "1"]
            |> tuples n // all possible data tuples
            |> permutations // and all possible permutations of these datasets
            ||> SplitYesNo // create all possible training sets

        let duplicatesRemoved =
            allPossible
            |||> Set.ofList // delete all duplicates, e.g. yes and no swapped, order of data in yes or no (training being symmetric)
            ||> Set.ofList
            |> List.distinct
            |||> Set.toList
            ||> Set.toList

        let symmetriesRemoved =
            duplicatesRemoved
                |> List.distinctBy (fun instances ->
                    let withSymmetries = symmetries ||> (fun s -> instances |||> permute s)
                    Set.ofList withSymmetries
                )

        symmetriesRemoved
            ||||> join ""
            ||||> Data.FromString
            ||> fun sets -> { Data.Yes = sets.[0]; Data.No = sets.[1] }

    // randomized version
    // for big n, it is unlikely to have symmetries, so we ignore that check
    let SomeDataSets n count min max =
        let max = Math.Min(pown 2 (n-1), max) + 1 // guarantee no overlap between yes and no dataset
        let allWords = ["0"; "1"] |> tuples n |> List.toArray

        [ for _ in 1..count ->
            shuffle allWords |> ignore
            let yes =
                allWords
                |> Array.take (rand.Next(min, max))
                |> Array.toList
                ||> join ""
            let no =
                allWords
                |> Array.rev
                |> Array.take (rand.Next(min, max))
                |> Array.toList
                ||> join ""
            { Data.Yes = Data.FromString <|| yes; Data.No = Data.FromString <|| no }
         ]
    
    // utility function for running a batch of simulations as a cartesian product over all parameters
    type RunParameters =
        {
            trainOnQudits : int
            maxVertices : int
            trotter: int
            resolution: int
        }
    let DefaultRunParameters =
        {
            trainOnQudits = 4
            maxVertices = 4
            trotter = 20
            resolution = 20
        }
    let SingleControlRunParameters = 
        {
            DefaultRunParameters with
                trainOnQudits = 20 // 2^20 interactions, practically infinite
                maxVertices = 25 // limit is 23 anyway
        }
    let IndividualControlRunParameters =
        {
            DefaultRunParameters with
                trainOnQudits = 1
                maxVertices = 25 // as large as the original interaction is anyway
        }

    open LiquidLearn.Graph
    open LiquidLearn.Interactions
    let Run
        (
            tag: string,
            parameters : RunParameters list
        )
        (
            datasets : (Data.DataSet * Data.DataSet) list,
            edges : EdgeT list,
            interactions : Sets.IInteractionFactory list
        ) =

        let graph = new Hypergraph(edges)

        // iterate over all parameters and interaction sets
        for par in parameters do
            for interaction in interactions do
                let filename =
                    sprintf "%s-%d-%d-%d-%d-graph-%s-%s-data-%d"
                        tag
                        par.trainOnQudits par.maxVertices par.trotter par.resolution
                        graph.ShortForm
                        interaction.ShortForm
                        datasets.Length 

                if (not <| File.Exists(filename + ".lock")) && (not <| File.Exists(filename + ".stats")) then
                    File.WriteAllText(filename + ".lock", sprintf "%s" System.Environment.MachineName)
                    if File.Exists(filename + ".test") then File.Delete(filename + ".test")

                    let stopwatch = Stopwatch.StartNew()

                    let model = new Train.MultiControlModel(
                                    graph,
                                    interaction,
                                    trainOnQudits = par.trainOnQudits,
                                    maxVertices = par.maxVertices,
                                    trotter = par.trotter,
                                    resolution = par.resolution
                                )
                    let timeModelCreated = stopwatch.ElapsedMilliseconds
                   
                    if model.Size > 23 then
                        dumps (sprintf "too many qubits needed: %d. Aborting." model.Size)
                        File.WriteAllText(filename + ".fail", sprintf "too many qubits needed: %d" model.Size)
                    else
                        // run model for every training set
                        datasets
                            ||> fun (train, test) ->
                                    let results = (model.Train train).Test test
                                    results.ToFile(filename + ".test", append = true)
                                    results
                            ||> Dump |> ignore
                        let timeDone = stopwatch.ElapsedMilliseconds

                        // write stats file
                        File.WriteAllText(
                                filename + ".stats",
                                sprintf "computer %s\ntimeModelCreated %d\ntimeDone %d\narg per run %d\n%A\n%A\n%A\n\n%A\n\n%A"
                                    System.Environment.MachineName
                                    timeModelCreated
                                    timeDone
                                    (timeDone/(int64 datasets.Length))
                                    graph
                                    par
                                    model.TrainingGraph
                                    interaction
                                    datasets
                            )

                    File.Delete(filename + ".lock")

                            


    [<LQD>]
    let MSRSubmissionData(mode : string) =
        match mode with
        | "benchmark" ->
            // training data
            let symmetry2 = permutations [1; 2] ||> fun a -> [1; 2], a
            let symmetry3 = permutations [1; 2; 3] ||> fun a -> [1; 2; 3], a

            let dataSets2 = AllDataSets 2 symmetry2 |> fun a -> List.zip a a
            let dataSets3 = AllDataSets 3 symmetry3 |> fun a -> List.zip a a

            let dataSets6 = SomeDataSets 6 10 5 25 |> fun a -> List.zip a a

            // batch list of runs
            let runs : ((Data.DataSet*Data.DataSet) list * EdgeT list * Sets.IInteractionFactory list) list = [
                dataSets2, [ O"1" --- O"2" ], [Sets.Projectors(); Sets.Paulis(); Sets.Heisenberg(); Sets.Ising(); Sets.Random()]
                dataSets2, [ O"1" --- V"h"; V"h" --- O"2" ], [Sets.Projectors(); Sets.Paulis(); Sets.Heisenberg(); Sets.Ising(); Sets.Random()]
                dataSets3, [ O"1" --- V"h"; V"h" --- O"2"; V"h" --- O"3" ], [Sets.Projectors(); Sets.Heisenberg(); Sets.Ising(); Sets.Random(); Sets.Paulis()]
            
                dataSets6, [ O"1" --- V"a"; V"a" --- O"4"; V"a" --- O"2"; V"a" --- V"b"; V"a" --- O"5"; O"2" --- V"b"; O"5" --- V"b"; V"b" --- O"3"; V"b" --- O"6"], [ Sets.Projectors(); Sets.Ising(); Sets.Heisenberg()  ]
            ]

            // run
            runs ||> (Run ("benchmark", [DefaultRunParameters])) |> ignore

        | "color" ->
            let edges = [
                O"r1" --- V"c"
                O"r2" --- V"c"
                O"r3" --- V"c"

                O"g1" --- V"c"
                O"g2" --- V"c"
                O"g3" --- V"c"

                O"b1" --- V"c"
                O"b2" --- V"c"
                O"b3" --- V"c"
            ]
            Run ("color", [DefaultRunParameters]) ([LearnAppData.Color.Training3bit, { Yes = LearnAppData.Color.Validate3bit; No = [] }], edges, [Sets.Projectors()])

        | _ ->
            dumps "specify either \"benchmark\" or \"color\" as argument to MSRSubmissionData()"

        ()

    [<LQD>]
    let QTMLData(what: string, qudits: int, verts: int) =
        let MyRunParameters = [
                for t in [5; 10; 20; 30; 40; 50; 75; 100; 125; 150] do
                for r in [5; 10; 20; 30; 40; 50; 75; 100; 125; 150] do
                    yield { DefaultRunParameters with
                                trainOnQudits = qudits
                                maxVertices = verts
                                trotter = t
                                resolution = r
                    }
            ]

        match what with
        | "test" ->
            let data = (AllDataSets 2 []) |> List.take 3
            let edges = [ O"1" --- O"2" ]
            let graph = new Hypergraph(edges)
            let model = new Train.MultiControlModel(graph, Sets.ReducedProjectors(), trainOnQudits = 2, maxVertices = 2, avoidSplittingEdges = true, resolution = 50)

            data
                ||> fun d -> 
                    let trained = model.Train d
                    let results = model.Test d
                    results
                |> Dump
                |> ignore

            ()
        | "color small" ->
            let edges = [
                O"r1" --- V"c"
                O"r2" --- V"c"

                O"g1" --- V"c"
                O"g2" --- V"c"

                O"b1" --- V"c"
                O"b2" --- V"c"
            ]
            Run ("color-small", MyRunParameters) ([LearnAppData.Color.Training2bit, { Yes = LearnAppData.Color.Validate2bit; No = [] }], edges, [Sets.Projectors(); Sets.ReducedProjectors()])

        | "color large" ->
            let edges = [
                O"r1" --- V"cr"
                O"r2" --- V"cr"
                O"r3" --- V"cr"

                O"g1" --- V"cg"
                O"g2" --- V"cg"
                O"g3" --- V"cg"

                O"b1" --- V"cb"
                O"b2" --- V"cb"
                O"b3" --- V"cb"

                V"cr" --- V"c"
                V"cg" --- V"c"
                V"cb" --- V"c"
            ]
            Run ("color-large", MyRunParameters) ([LearnAppData.Color.Training3bit, { Yes = LearnAppData.Color.Validate3bit; No = [] }], edges, [Sets.Projectors()])

        | _ ->
            dumps "specify either \"color small\" or \"color large\" as argument to QTMLData()"

        ()

    [<LQD>]
    let QTMLDataMCMvsSCM(model : string) =
        let graph =
            new Hypergraph(
              [
                O"r1" --- V"c"
                O"r2" --- V"c"
                O"r3" --- V"c"

                O"g1" --- V"c"
                O"g2" --- V"c"
                O"g3" --- V"c"

                O"b1" --- V"c"
                O"b2" --- V"c"
                O"b3" --- V"c"
              ]
            )
        let trainData = LearnAppData.Color.Training3bit
        let testData = { Data.DataSet.Yes = LearnAppData.Color.Validate3bit; Data.DataSet.No = [] }
        let baseName = sprintf "MCMvsSCM.%s" System.Environment.MachineName

        match model with
        | "MCM" ->
            let trainer =
              new Train.MultiControlModel(
                graph,
                Sets.Projectors(),
                trainOnQudits = 4,
                maxVertices = 4,
                avoidSplittingEdges = true,
                trotter = 20,
                resolution = 50
              )
            let trained = trainer.Train trainData
            (trained.Test testData).ToFile(sprintf "%s.MCM.test" baseName)
        | "SCM" ->
            let trainer =
              new Train.SingleControlModel(
                graph,
                Sets.Projectors(),
                controlCount = 3,
                trotter = 20,
                resolution = 20
              )
            let trained =
              trainer.Train(
                trainData,
                epochs=500,
                testInjector =
                    fun (epoch, partiallyTrained) ->
                        if true then
                            (partiallyTrained.Test testData).ToFile(sprintf "%s.SCM.%d.test" baseName epoch)
                        ()
              )
            (trained.Test testData).ToFile(sprintf "%s.SCM.test" baseName)
        | _ ->
            dumps "specify either \"MCM\" or \"SCM\" as argument to QTMLDataMCMvsSCM()"

        ()

    [<LQD>]
    let HelloWorld() =
        let data = (AllDataSets 2 []) |> List.take 3
        let edges = [ O"1" --- O"2" ]
        let graph = new Hypergraph(edges)
        let model = new Train.MultiControlModel(graph, Sets.Projectors(), trainOnQudits = 2, maxVertices = 3, avoidSplittingEdges = true, resolution = 50, trotter = 20)

        data
            ||> fun d -> 
                let trained = model.Train d
                let results = trained.Test d
                results
            |> Dump
            |> ignore

        ()



    [<EntryPoint>]
    let main _ =
        let echo = dumpsWithColor System.ConsoleColor.Red
        echo "╦  ┬┌─┐ ┬ ┬┬┌┬┐╦  ┌─┐┌─┐┬─┐┌┐┌"
        echo "║  ││─┼┐│ ││ ││║  ├┤ ├─┤├┬┘│││"
        echo "╩═╝┴└─┘└└─┘┴─┴┘╩═╝└─┘┴ ┴┴└─┘└┘"
        echo "(c) 2016, 2017 Johannes Bausch (jkrb2@cam.ac.uk)"

        dumps (sprintf ".NET runtime %A" System.Environment.Version)

        // tone down Liquid's own output
        System.Console.ForegroundColor <- if Type.GetType("Mono.Runtime") <> null then System.ConsoleColor.Gray else System.ConsoleColor.DarkGray 
        App.RunLiquid()