﻿// LiquidLearn (c) 2016, 2017 Johannes Bausch
namespace LiquidLearn.Train

open Microsoft.Research

open LiquidLearn
open LiquidLearn.Utils
open LiquidLearn.Train

type SingleControlModel
    (
        graph : Graph.Hypergraph,
        interactions : Interactions.Sets.IInteractionFactory,
        ?controlCount : int,
        ?trotter : int,
        ?resolution : int
    ) = class
    inherit Train.Model(
        "Single Control Model",
        graph,
        interactions,
        ?trotter = trotter,
        ?resolution = resolution
    )
    let controlCount = defaultArg controlCount 3

    do
        dumps (sprintf "controlCount=%d" controlCount)
        dump graph

    // original graph edges
    let edges = graph.Edges

    // all couplings as a StaticEdgeT array
    let mutable graphInteractions = (
        edges
        ||> interactions.ListPossibleInteractions
        |> List.concat
        |> List.toArray
    )

    // generates a training graph
    member private this.SampleTrainingGraph() =
        let interactionsToControl = ( (shuffle graphInteractions) |> Array.truncate controlCount ) |> Array.toList
        let controlEdge = Graph.CE interactionsToControl
        let remainingEdges = [ for e in List.except interactionsToControl (Array.toList graphInteractions) -> Graph.SE e ]

        // build training graph extended by control
        new Graph.Hypergraph(controlEdge :: remainingEdges)


    // train graph with given data
    member this.Train (data : Data.DataSet, ?epochs : int, ?testInjector : int*SingleControlModel -> unit, ?threshold : float, ?postSelector : string -> Liquid.Ket -> Liquid.Ket) =
        let epochs = defaultArg epochs 10
        let testInjector = defaultArg testInjector (fun _ -> ())
        let threshold = defaultArg threshold 0.01
        this.Annealer.PostSelector <- defaultArg postSelector (fun _ ket -> ket)

        for epoch in 1..epochs do
            let trainingGraph = this.SampleTrainingGraph()

            dumps (sprintf "SCM epoch %d on %A" epoch trainingGraph)
            let results = this.Annealer.TrainSeparate("SCM", trainingGraph, data, (1.0, 1.0))

            // set parameters in trained graph
            this.SetTrainedGraph(results, Array.toList graphInteractions)

            // inject test
            testInjector(epoch, this)

            // get those interaction parameters out for next epoch
            let allEdges = this.TrainedGraph.Value.Edges ||> function | Graph.SE e -> e | _ -> failwith "never happens"
            graphInteractions <-
            [|
                for e in allEdges do
                    // regularize amplitudes away from zero
                    let a = e.amplitude
                    let a = if (abs a) < threshold then (sign a |> float)*threshold else a
                    yield { e with amplitude = a }
                done
            |]
        done

        this

end
