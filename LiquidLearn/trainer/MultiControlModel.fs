﻿// LiquidLearn (c) 2016, 2017 Johannes Bausch
namespace LiquidLearn.Train

open Microsoft.Research

open LiquidLearn
open LiquidLearn.Utils
open LiquidLearn.Train

// multiple controlled gate trainer
type MultiControlModel
    (
        graph : Graph.Hypergraph,
        interactions : Interactions.Sets.IInteractionFactory,
        ?trainOnQudits : int,
        ?maxVertices : int,
        ?avoidSplittingEdges : bool,
        ?trotter : int,
        ?resolution : int
    ) = class
    inherit Train.Model(
        "Multi Control Model",
        graph,
        interactions,
        ?trotter = trotter,
        ?resolution = resolution
    )

    let trainOnQudits = defaultArg trainOnQudits 2
    let maxVertices = defaultArg maxVertices 3
    let avoidSplittingEdges = defaultArg avoidSplittingEdges true

    do
        dumps (sprintf "trainOnQudits=%s" (if trainOnQudits > 1 then (sprintf "yes (%d)" trainOnQudits) else "no"))
        dump graph

    // create controlled version of graph
    let trainingGraph =
        (new Graph.Hypergraph(
            [ for e in graph.Edges ->
                interactions.ListPossibleInteractions e |> Graph.CE
            ])
        ).OptimizeControls(
            maxInteractions = (pown 2 trainOnQudits) - 1,
            maxVertices = maxVertices,
            avoidSplittingEdges = avoidSplittingEdges
        )   

    do
        dumps (sprintf "training on %A" trainingGraph)
        dumps (sprintf "with interaction set %A" interactions)

    // return training graph
    member this.TrainingGraph with get() = trainingGraph

    // return size of training graph
    member this.Size = trainingGraph.Size

    // train dataset
    member this.Train (data : Data.DataSet, ?postSelector : string -> Liquid.Ket -> Liquid.Ket) =
        this.Annealer.PostSelector <- defaultArg postSelector (fun _ ket -> ket)

        let results = this.Annealer.TrainSeparate("MCM", trainingGraph, data, (1.0, 1.0))
        this.SetTrainedGraph(results)        
        this
end
