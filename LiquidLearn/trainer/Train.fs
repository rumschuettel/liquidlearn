﻿// LiquidLearn (c) 2016, 2017 Johannes Bausch
namespace LiquidLearn.Train

open Microsoft.Research
open Liquid.Operations // for >< and !! operator

open LiquidLearn
open LiquidLearn.Utils
open LiquidLearn.Utils.Data

module ControlMeasurement =
    // extract measurement statistics from a list of qubits
    let MeasurementStatistics (ket : Liquid.Ket) (qubits : int list) =
        ket.Probs !!(ket.Qubits, List.rev qubits) |> Array.toList


    // interpret measurement result on controlled interaction
    let InterpretControlMeasurement (results : float list) (control : Graph.ControlledEdgeT) =
        let interactionCount = (Graph.CE control).HowManyInteractions
        let totalBlocks = nextPowerOf2 (interactionCount + 1)

        // for e.g. 5 interactions, we reserved 3 qubits, so totalBlocks = 8; which should equal the measured qubit list (2^3=8)
        assert (totalBlocks = results.Length)

        // number of identity blocks in the top left
        let controlBlocks = (totalBlocks - interactionCount)

        // support on control
        let zeroInteractionSupport =
            results
            |> List.take controlBlocks
            |> List.sum

        let results =
            results
            |> List.skip controlBlocks
            ||> (fun prob -> (prob - zeroInteractionSupport) * (float interactionCount) / 2.0)


        List.zip control results

[<StructuredFormatDisplay("TestResults {TableForm}")>]
type TestResults = {
        YesStats : ((float*float)*float) list
        NoStats  : ((float*float)*float) list
}  with
    member this.TableForm =
        join "" (
            [ for ((e, sigma), p) in this.YesStats -> sprintf "\n  y\t%.2e\t%.2e\t%.2e" e sigma p ]
            @
            [ for ((e, sigma), p) in this.NoStats -> sprintf "\n  n\t%.2e\t%.2e\t%.2e" e sigma p ]
        )

    member this.ToFile (filename, ?append) =
        let append = defaultArg append false
        if append then
            System.IO.File.AppendAllText(filename, this.TableForm)
        else
            System.IO.File.WriteAllText(filename, this.TableForm)

// Annealing interface
type Annealer
    (
        factory: Interactions.Sets.IInteractionFactory,
        schedule: (int * float []) list,
        trotter,
        resolution
    ) = class

    member val PostSelector = (fun _ ket -> ket) with get, set

    member private this.Anneal (tag, qubits, couplings) =
        // create spin model and train
        dumps (sprintf "allocating %d qubits" qubits)
        let spin = new Liquid.Spin(couplings, qubits, Liquid.RunMode.Trotter1X)
        Liquid.Spin.Test(
            tag = tag,
            repeats = 1,
            trotter = trotter,
            schedule = schedule,
            res = 12*resolution,
            spin = spin,
            runonce = true,
            decohereModel = []
        )

        // allow postselection function, also used for test injections
        (spin, this.PostSelector tag spin.Ket)

    member this.Train (tag, graph: Graph.Hypergraph, data: Data.DataSet, weights: float * float) =
        // interactions to train
        let couplings = [
            for edge in graph.Edges do
                yield new Liquid.SpinTerm(
                    s = 2,
                    o = factory.Interaction edge,
                    idx = graph.WhichQubits edge,
                    a = 1.0
                )
        ]
        // projector onto training data
        let projector =
            new Liquid.SpinTerm(
                s = 1,
                o = Interactions.Projector (graph.Outputs.Length) (data.ValuatedList weights),
                idx = graph.WhichQubits graph.Outputs,
                a = float graph.Size // scale amplitude with size of graph to train
            )
        
        // anneal using projector onto training data and the controlled interactions
        let _, ket = this.Anneal(tag, graph.Size, projector :: couplings)

        // extract trained control probabilities
        [ for edge in graph.Edges do
            match edge with
            | Graph.CE e ->
                let results = ControlMeasurement.MeasurementStatistics ket ((graph.WhichControlQubits edge))
                yield ControlMeasurement.InterpretControlMeasurement results e
            | _ -> ()
        ]

    member this.TrainSeparate(tag, graph: Graph.Hypergraph, data: Data.DataSet, weights: float * float) =
        // run training once for YES and once for NO-instances
        // the reason for this choice is from the intuition that an energy bonus
        // makes the ground state localize much more than an energy penalty,
        // where we work under the assumption that there is little training data
        // compared to the overall Hilbert space.

        // weights before for each controlled edge
        let overallWeightsBefore = (graph.Controls |||> fun c -> abs c.amplitude) ||> List.sum

        ((sprintf "%s YES" tag, data, (fst weights, 0.0)), (sprintf "%s NO" tag, data, (0.0, snd weights)))
        ||>
        ( fun (tag, data : Data.DataSet, weight) ->
            // projector on training data. Interaction strength scaled to be dominating term
            dumps (sprintf "training %s (weights %.2f, %.2f) using %A" tag (fst weight) (snd weight) data)

            this.Train(tag, graph, data, weight)
        )
        |> (fun (yes, no) ->
            // type of yes and no is of type (StaticEdgeT * float) list list
            // each sub-list stems from one single control, of type
            // [edge1, weight1; edge2, weight2; ...]
            // we need to normalize the strengths by the number of interactions controlled by each.
            let mergedWeights =
                List.zip yes no
                ||> fun (y, n) -> List.zip3 (fst <|| y) (snd <|| y) (snd <|| n)

            let addedWeights =
                mergedWeights
                // now of type [edge1, weight1_yes, weight1_no; ...]
                ||> fun list -> (list ||> fun (e, wyes, wno) -> (e, wno - wyes))
                // now of type [edge1, weight1; edge2, weight2; ...]


            // normalize such that each controlled edge has an overall weight as before
            let renormalizedWeights = 
                List.zip addedWeights overallWeightsBefore
                ||> fun (list, weightBefore) ->
                        let weightAfter = list ||> snd ||> abs |> List.sum
                        let weightAfter = if weightAfter = 0.0 then 1.0 else weightAfter
                        list ||> fun (e, w) -> (e, w / weightAfter * weightBefore)
                |> List.concat
                                           
            renormalizedWeights
        )        

    member this.Test (graph: Graph.Hypergraph, data: Data.DataSet) =
        // build interactions from previously trained model
        // note that the trainer has to assemble this list this itself from the weights returned in Train
        let couplings = [
            for edge in graph.Edges do
                match edge with
                | Graph.SE e ->
                    yield new Liquid.SpinTerm(
                        s = 1,
                        o = factory.Interaction edge,
                        idx = graph.WhichQubits edge,
                        a = e.amplitude
                    )
                | _ -> failwith "test graph should only have static edges"
        ]

        //

        let spin, _ = this.Anneal("test", graph.Size, couplings)
        let state = new Liquid.Ket(graph.Size)
        let qubits = state.Qubits
        let outputs = graph.WhichQubits graph.Outputs // list of qubits that act as output qubits
        
        // 1. ground state overlap
        let probs = ControlMeasurement.MeasurementStatistics spin.Ket outputs

        // 2. energy expectation
        // set part of state vector that is not output to |0> + |1>, so that we partially trace out the hidden part
        for i in 0..graph.Size-1 do
            if not (contains i outputs) then
                qubits.[i].StateSet(1., 0., 1., 0.)

        let updateStateData (data : Data.DataT) =
            for i in 0..outputs.Length-1 do
                match data.[i] with
                | Data.BitT.Zero -> qubits.[outputs.[i]].StateSet(Liquid.Zero)
                | Data.BitT.One -> qubits.[outputs.[i]].StateSet(Liquid.One)
                | Data.BitT.Indeterminate -> qubits.[outputs.[i]].StateSet(1., 0., 1., 0.)
                | _ -> failwith "invalid bit type"
        
        // build statistics
        let yesStats = [
            for yes in data.Yes -> 
                updateStateData yes
                (
                    spin.EnergyExpectation( stdev = true, qubits = state.Qubits ),
                    probs.[ Data.ToNumber(yes) ]
                )
        ]
        let noStats = [
            for no in data.No -> 
                updateStateData no
                (
                    spin.EnergyExpectation( stdev = true, qubits = state.Qubits ),
                    probs.[ Data.ToNumber(no) ]
                )
        ]

        { YesStats = yesStats; NoStats = noStats }
end


// base class for trainers
type Model
    (
        name : string,
        graph : Graph.Hypergraph,
        interactions : Interactions.Sets.IInteractionFactory,
        ?trotter : int,
        ?resolution : int
    ) = class

    let mutable trainedGraph = None
    let trotter = defaultArg trotter 5
    let resolution = defaultArg resolution 30

    do
        Dumps name
        dumps (sprintf "trotter=%d\nresolution=%d" trotter resolution)

    // annealing schedule
    let schedule = [
        0,              [|1.00; 0.00; 0.00|];
        2*resolution,   [|1.00; 0.25; 0.00|];
        12*resolution,  [|0.00; 1.00; 1.00|]
    ]

    let annealer = Annealer(interactions, schedule, trotter, resolution)
    member this.Annealer = annealer

    // so that children can set graph from result
    member this.TrainedGraph with get() = trainedGraph
    member this.SetTrainedGraph(results : (Graph.StaticEdgeT * float) list, ?toMerge : Graph.StaticEdgeT list, ?learningRate : float) =
        // combine training results with edges and flatten,
        let trainedEdges =
            results
            ||> fun (edge, weight) ->
                {
                    Graph.StaticEdgeT.name = edge.name
                    Graph.StaticEdgeT.amplitude = weight
                    Graph.StaticEdgeT.vertices = edge.vertices
                }


        // if toMerge is given, merge new edges with old ones with a weight given by learningRate
        let learningRate = defaultArg learningRate 0.2
        let allEdges =
            match toMerge with
            | Some edges ->
                [ for e in edges ->
                    match List.tryFind (fun (ee : Graph.StaticEdgeT) -> (ee.name, ee.vertices) = (e.name, e.vertices)) trainedEdges with
                    | Some ee ->
                        { e with amplitude = (1.0-learningRate)*e.amplitude + learningRate*ee.amplitude }
                    | None -> e
                ]
            | None _ -> trainedEdges

        // normalize overall amplitudes
        let totalAmplitudes = (allEdges ||> fun e -> e.amplitude) ||> abs |> List.sum
        let totalAmplitudes = if totalAmplitudes = 0.0 then 1.0 else totalAmplitudes
        let factor = (float allEdges.Length) / totalAmplitudes
        let allEdges = allEdges ||> fun e -> { e with amplitude = e.amplitude * factor }

        trainedGraph <- Some(new Graph.Hypergraph(Graph.SE <|| allEdges))
        dumps "trained interaction graph"
        trainedGraph.Value |> dump

    // standard annealing test
    member this.Test (data : Data.DataSet) =
        match trainedGraph with
        | Some g -> annealer.Test(g, data)
        | None -> failwith "model not trained"


end
