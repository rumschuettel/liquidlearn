// LiquidLearn (c) 2016, 2017 Johannes Bausch
// Auto-Generated File with gates.nb on Wed 18 Oct 2017 11:44:29
namespace LiquidLearn.GeneratedCode

module ReducedProjectionMatrices =
    open Microsoft.Research.Liquid
    let List = [ "flip"; "00"; "11"; ]

    let Get (id : string) t =
        // local mapping
        let Cos = System.Math.Cos
        let Sin = System.Math.Sin
        let Cosh = System.Math.Cosh
        let Sinh = System.Math.Sinh
        let Power(a, b) = a**b
            
        match id with
        | "flip" -> CSMat(4, [0, 0, 1., 0.; 1, 1, Cos(t), Sin(t); 2, 2, Cos(t), -1.*Sin(t); 3, 3, 1., 0.])
        | "00" -> CSMat(4, [0, 0, Cos(t), Sin(t); 1, 1, 1., 0.; 2, 2, 1., 0.; 3, 3, 1., 0.])
        | "11" -> CSMat(4, [0, 0, 1., 0.; 1, 1, 1., 0.; 2, 2, 1., 0.; 3, 3, Cos(t), Sin(t)])
        | _ -> failwith (sprintf "Reduced Projection %s not precomputed" id)
